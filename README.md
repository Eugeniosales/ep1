# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

*1)* A parte inicial do programa requisita a quantidade de eleitores, dos quais não será mais permitido votar após o atingimento desse número.

*2)* No Menu Inicial são apresentadas 4 opções:

	- Votação: Será requisitado o nome do eleitor e um voto para cada cargo político, sendo apresentado outras funionalidades da urna como o voto em branco, apagar caracteres, cancelar e etc.
	
	- Relatorio: Constarão o nome dos eleitores até o momento da votação, bem como seus votos. Se não houver nenhuma votação anterior, é exibido uma menssagem alegando o mesmo.
	
	- Listagem: É requisitado o código do canditado e será exibido informações pessoais como cargo, partido, ocupação, cidade de nascimento e etc. Tanto para o candidato principal quanto para o vice, uma vez que seus códigos são análogos.
	
	- Resultado: O resultado estará disponível uma vez que a quantidade de votos chegar a quantidade pré-estabelecidada de eleitores. Nessa seção, será apresentado o nome, cargo e quantidade de votos dos candidatos ganhadores.
	
	- Sair: Para abortar a votação e o consequentemente o programa.

## Bugs e problemas

* Devido algumas dificuldades inciais com o git (Comecei o repositório com git init, em vez de clonar) e algumas tentativas falha de transferir os commits, optei por comitar, apenas, no novo repositório clonado. O que implicou na perda de 8 commits desde 8 de setembro. Mas a regularidade de execução do projeto foi respeitada, bem como toda documentação e requisitos.

* Na sessão votação, quando requisitado o nome do eleitor, digite, apenas o primeiro nome ou um dos sobrenomes.

## Referências
