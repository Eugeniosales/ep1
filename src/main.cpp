#include <cstdlib>
#include <string>
#include "Dados.hpp"
#include "urna.hpp"
#include "leitura.hpp"

int main()
{
	string **candidatos_br = NULL, **candidatos_df = NULL;
	
	candidatos_br = leitura("data/consulta_cand_2018_BR.csv");
	candidatos_df = leitura("data/consulta_cand_2018_DF.csv");
	Dados *politicos = setters(candidatos_br, candidatos_df);
	loop_votacao(politicos);
	desaloca_vetor(politicos);

        return 0;
}
