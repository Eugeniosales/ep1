#include "leitura.hpp"
#include <stdexcept>

string **aloca_memoria(int l, int c)
{
	string **ptr = NULL;

	try{
   		ptr = new string*[l];

    		for(int i = 0; i < l; i++)
   		 {
   		 	ptr[i] = new string[c];
  		 }

  	 	if(ptr == NULL)
  	 		throw "Nao foi possivel realizar a alocação dinamica";

    		}catch(const char* e){

		 	 cout << "ERRO: " << e << endl;
		}

	return ptr;
}

void desaloca_memoria(string **ptr, int l)
{
    for(int i = 0; i < l; ++i)
    {
	delete [] ptr[i];
	}

	delete [] ptr;
}

int *string_to_int(string **codigo, int qtd_candidatos, int pos)
{
	int *codigo_candidato = new int[qtd_candidatos];

	for(int i = 0; i < qtd_candidatos; i++)
		codigo_candidato[i] = 0;

	for(int k = 0; k < qtd_candidatos; k++){

		int j = 0;
		for(int i = codigo[k][pos].size() - 1; i >= 0; i--)
		{
			codigo_candidato[k] += (codigo[k][pos][i] - '0') * pow(10, j);
			j++;
		}
	}

	return codigo_candidato;
}

string **leitura(const char *caminho)
{
	ifstream ptr;
	string **data = NULL;
	vector <string> buffer (1240);
	int linhas = 0, colunas = 0;
	int k, d;


	try{
		ptr.open(caminho);

		if(caminho == NULL)
		{
			throw "O Arquivo nao abriu";
		}

		if(ptr == NULL)
		{
			throw "Ptr esta nulo";
		}
	}catch(const char* e)
	{
		cout << "ERRO: " << e << endl;
	}


	getline(ptr, buffer[0]); //Mover cursor para segunda linha

	while(getline(ptr, buffer[linhas])) linhas++; // Linhas

	for(int q = 0; buffer[0][q] != '\0'; q++)
	{
		if(buffer[0][q] == ';') colunas++; //colunas
	}

	data = aloca_memoria(linhas, colunas);

	for(k = 0; k < linhas; k++){

		int i = 0;

		for(d = 0; buffer[k][d] != '\0'; d++)
		{
			int delimitador = buffer[k].find(';');

			for(int j = 0; j < delimitador; j++)
			{
				if(buffer[k][j] != '"')
					*(*(data+k)+i) += buffer[k][j];
			}

			buffer[k].erase(0, delimitador+1);
			i++;
		}
	}
	ptr.close();

	return data;
}

string **concatena_politicos(string **cand_br, string **cand_df, int lines_br, int total, int columns)
{
	string **data = aloca_memoria(total, columns);

	for(int i = 0; i < total; i++){
		for(int j = 0; j < columns; j++)
		{
			if(i < 26)
				*(*(data+i)+j) = *(*(cand_br+i)+j);
			else
				*(*(data+i)+j) = *(*(cand_df+i-lines_br)+j);
		}
	}

	return data;
}

Dados *setters(string **cand_br, string **cand_df)
{

	int rows_df = 1237;
	int rows_br = 26;
	int total_politicos = rows_df + rows_br;
	Dados *politicos = new Dados[total_politicos];
	string **data = concatena_politicos(cand_br, cand_df, rows_br, total_politicos, 57);
	int *codigo_candidatos = string_to_int(data, total_politicos, 16); //Para converter de string em inteiro
	int *cargo_candidatos = string_to_int(data, total_politicos, 13);

	for(int i = 0; i < total_politicos; i++)
	{
		politicos[i].set_DT_GERACAO( *(*(data+i)+0));
		politicos[i].set_HH_GERACAO( *(*(data+i)+1));
		politicos[i].set_ANO_ELEICAO( *(*(data+i)+2));
		politicos[i].set_CD_TIPO_ELEICAO( *(*(data+i)+3));
		politicos[i].set_NM_TIPO_ELEICAO( *(*(data+i)+4));
		politicos[i].set_NR_TURNO( *(*(data+i)+5));
		politicos[i].set_CD_ELEICAO( *(*(data+i)+6));
		politicos[i].set_DS_ELEICAO( *(*(data+i)+8));
		politicos[i].set_DT_ELEICAO( *(*(data+i)+9));
		politicos[i].set_TP_ABRANGENCIA( *(*(data+i)+10));
		politicos[i].set_SG_UF( *(*(data+i)+11));
		politicos[i].set_SG_UE( *(*(data+i)+11));
		politicos[i].set_NM_UE( *(*(data+i)+12));
		politicos[i].set_CD_CARGO(cargo_candidatos[i]);
		politicos[i].set_DS_CARGO( *(*(data+i)+14));
		politicos[i].set_SQ_CANDIDATO( *(*(data+i)+15));
		politicos[i].set_NR_CANDIDATO( codigo_candidatos[i]);
		politicos[i].set_NOME( *(*(data+i)+17));
		politicos[i].set_NM_URNA_CANDIDATO( *(*(data+i)+18));
		politicos[i].set_NM_SOCIAL_CANDIDATO( *(*(data+i)+19));
		politicos[i].set_NR_CPF_CANDIDATO( *(*(data+i)+20));
		politicos[i].set_NM_EMAIL( *(*(data+i)+21));
		politicos[i].set_CD_SITUACAO_CANDIDATURA( *(*(data+i)+22));
		politicos[i].set_CD_SITUACAO_CANDIDATURA( *(*(data+i)+23));
		politicos[i].set_CD_DETALHE_SITUACAO_CAND( *(*(data+i)+24));
		politicos[i].set_DS_DETALHE_SITUACAO_CAND( *(*(data+i)+25));
		politicos[i].set_TP_AGREMIACAO( *(*(data+i)+26));
		politicos[i].set_NR_PARTIDO( *(*(data+i)+27));
		politicos[i].set_SG_PARTIDO( *(*(data+i)+28));
		politicos[i].set_NM_PARTIDO( *(*(data+i)+29));
		politicos[i].set_SQ_COLIGACAO( *(*(data+i)+30));
		politicos[i].set_NM_COLIGACAO( *(*(data+i)+31));
		politicos[i].set_DS_COMPOSICAO_COLIGACAO( *(*(data+i)+32));
		politicos[i].set_CD_NACIONALIDADE( *(*(data+i)+33));
		politicos[i].set_DS_NACIONALIDADE( *(*(data+i)+34));
		politicos[i].set_SG_UF_NASCIMENTO( *(*(data+i)+35));
		politicos[i].set_CD_MUNICIPIO_NASCIMENTO(*(*(data+i)+37));
		politicos[i].set_DT_NASCIMENTO( *(*(data+i)+38));
		politicos[i].set_NR_IDADE_DATA_POSSE( *(*(data+i)+40));
		politicos[i].set_NR_TITULO_ELEITORAL_CANDIDATO( *(*(data+i)+41));
		politicos[i].set_CD_GENERO( *(*(data+i)+42));
		politicos[i].set_DS_GENERO( *(*(data+i)+43));
		politicos[i].set_CD_GRAU_INSTRUCAO( *(*(data+i)+44));
		politicos[i].set_DS_GRAU_INSTRUCAO( *(*(data+i)+45));
		politicos[i].set_CD_ESTADO_CIVIL( *(*(data+i)+46));
		politicos[i].set_DS_ESTADO_CIVIL( *(*(data+i)+47));
		politicos[i].set_CD_COR_RACA( *(*(data+i)+48));
		politicos[i].set_DS_COR_RACA( *(*(data+i)+49));
		politicos[i].set_CD_OCUPACAO( *(*(data+i)+50));
		politicos[i].set_DS_OCUPACAO( *(*(data+i)+51));
		politicos[i].set_NR_DESPESA_MAX_CAMPANHA( *(*(data+i)+52));
		politicos[i].set_CD_SIT_TOT_TURNO( *(*(data+i)+53));
		politicos[i].set_DS_SIT_TOT_TURNO( *(*(data+i)+54));
		politicos[i].set_ST_REELEICAO( *(*(data+i)+55));
	}

	desaloca_memoria(cand_br,rows_br);
	desaloca_memoria(cand_df, rows_df);
	desaloca_memoria(data, total_politicos);
	delete [] codigo_candidatos;
	delete [] cargo_candidatos;

	return politicos;
}

void desaloca_vetor(Dados *p)
{
	delete [] p;
}
