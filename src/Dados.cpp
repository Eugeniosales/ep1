#include "Dados.hpp"
#include <cstdlib>


//Construtor
Dados::Dados()
{
	DT_GERACAO = ""; 	
	HH_GERACAO = "";	
	ANO_ELEICAO = "";	
	CD_TIPO_ELEICAO = "";	
	NM_TIPO_ELEICAO = "";
	NR_TURNO = "";
	CD_ELEICAO = "";	
	DS_ELEICAO = "";	
	DT_ELEICAO = "";	
	TP_ABRANGENCIA = "";	
	SG_UF = "";	
	SG_UE = "";	
	NM_UE = "";	
	CD_CARGO = 0;	
	DS_CARGO = "";	
	SQ_CANDIDATO = "";	
	NR_CANDIDATO = 0;	
	NOME = "";	
	NM_URNA_CANDIDATO = "";	
	NM_SOCIAL_CANDIDATO = "";	
	NR_CPF_CANDIDATO = "";	
	NM_EMAIL = "";	
	CD_SITUACAO_CANDIDATURA = "";	
	DS_SITUACAO_CANDIDATURA = "";	
	CD_DETALHE_SITUACAO_CAND = "";	
	DS_DETALHE_SITUACAO_CAND = "";	
	TP_AGREMIACAO = "";	
	NR_PARTIDO = "";	
	SG_PARTIDO = "";	
	NM_PARTIDO = "";	
	SQ_COLIGACAO = "";
	NM_COLIGACAO = "";	
	DS_COMPOSICAO_COLIGACAO = "";
	CD_NACIONALIDADE = "";	
	DS_NACIONALIDADE = "";	
	SG_UF_NASCIMENTO = "";	
	CD_MUNICIPIO_NASCIMENTO = "";	
	NM_MUNICIPIO_NASCIMENTO = "";	
	DT_NASCIMENTO = "";	
	NR_IDADE_DATA_POSSE = "";	
	NR_TITULO_ELEITORAL_CANDIDATO = "";	
	CD_GENERO = "";	
	DS_GENERO = "";	
	CD_GRAU_INSTRUCAO = "";	
	DS_GRAU_INSTRUCAO = "";	
	CD_ESTADO_CIVIL = "";	
	DS_ESTADO_CIVIL = "";	
	CD_COR_RACA = "";	
	DS_COR_RACA = "";	
	CD_OCUPACAO = "";	
	DS_OCUPACAO = "";	
	NR_DESPESA_MAX_CAMPANHA = "";	
	CD_SIT_TOT_TURNO = "";
	DS_SIT_TOT_TURNO = "";	
	ST_REELEICAO = "";	
	ST_DECLARAR_BENS = "";	
	NR_PROTOCOLO_CANDIDATURA = "";
	NR_PROCESSO = "";
}

//Destrutor
Dados::~Dados(){} 

//=======Getters e Setters===================

void Dados::set_DT_GERACAO(string DT_GERACAO)
{
        this ->DT_GERACAO = DT_GERACAO;
}
string Dados::get_DT_GERACAO()
{
        return DT_GERACAO;
}

void Dados::set_HH_GERACAO(string HH_GERACAO)
{
        this->HH_GERACAO = HH_GERACAO;
}
string Dados::get_HH_GERACAO()
{
        return HH_GERACAO;
}

void Dados::set_ANO_ELEICAO(string ANO_ELEICAO)
{
        this ->ANO_ELEICAO = ANO_ELEICAO;
}
string Dados::get_ANO_ELEICAO()
{
        return ANO_ELEICAO;
}

void Dados::set_CD_TIPO_ELEICAO(string CD_TIPO_ELEICAO)
{
        this->CD_TIPO_ELEICAO = CD_TIPO_ELEICAO;
}
string Dados::get_CD_TIPO_ELEICAO()
{
        return CD_TIPO_ELEICAO;
}
void Dados::set_NM_TIPO_ELEICAO(string NM_TIPO_ELEICAO)
{
        this ->NM_TIPO_ELEICAO = NM_TIPO_ELEICAO;
}
string Dados::get_NM_TIPO_ELEICAO()
{
        return NM_TIPO_ELEICAO;
}

void Dados::set_NR_TURNO(string NR_TURNO)
{
        this->NR_TURNO = NR_TURNO;
}
string Dados::get_NR_TURNO()
{
        return NR_TURNO;
}
void Dados::set_CD_ELEICAO(string CD_ELEICAO)
{
        this ->CD_ELEICAO = CD_ELEICAO;
}
string Dados::get_CD_ELEICAO()
{
        return CD_ELEICAO;
}

void Dados::set_DS_ELEICAO(string DS_ELEICAO)
{
        this->DS_ELEICAO = DS_ELEICAO;
}
string Dados::get_DS_ELEICAO()
{
        return DS_ELEICAO;
}
void Dados::set_DT_ELEICAO(string DT_ELEICAO)
{
        this ->DT_ELEICAO = DT_ELEICAO;
}
string Dados::get_DT_ELEICAO()
{
        return DT_ELEICAO;
}

void Dados::set_TP_ABRANGENCIA(string TP_ABRANGENCIA)
{
        this ->TP_ABRANGENCIA = TP_ABRANGENCIA;
}
string Dados::get_TP_ABRANGENCIA()
{
        return TP_ABRANGENCIA;
}
void Dados::set_SG_UF(string SG_UF)
{
        this ->SG_UF = SG_UF;
}
string Dados::get_SG_UF()
{
        return SG_UF;
}
void Dados::set_SG_UE(string SG_UE)
{
        this ->SG_UE = SG_UE;
}
string Dados::get_SG_UE()
{
        return SG_UE;
}
void Dados::set_NM_UE(string NM_UE)
{
        this ->NM_UE = NM_UE;
}
string Dados::get_NM_UE()
{
        return NM_UE;
}
void Dados::set_CD_CARGO(int CD_CARGO)
{
        this ->CD_CARGO = CD_CARGO;
}
int Dados::get_CD_CARGO()
{
        return CD_CARGO;
}
void Dados::set_DS_CARGO(string DS_CARGO)
{
        this ->DS_CARGO = DS_CARGO;
}
string Dados::get_DS_CARGO()
{
        return DS_CARGO;
}
void Dados::set_SQ_CANDIDATO(string SQ_CANDIDATO)
{
        this -> SQ_CANDIDATO = SQ_CANDIDATO;
}
string Dados::get_SQ_CANDIDATO()
{
        return SQ_CANDIDATO;
}
void Dados::set_NR_CANDIDATO(int NR_CANDIDATO)
{
        this ->NR_CANDIDATO = NR_CANDIDATO;
}
int Dados::get_NR_CANDIDATO()
{
        return NR_CANDIDATO;
}
void Dados::set_NOME(string NOME)
{
        this -> NOME = NOME;
}
string Dados::get_NOME()
{
        return NOME;
}

void Dados::set_NM_URNA_CANDIDATO(string NM_URNA_CANDIDATO)
{
	this -> NM_URNA_CANDIDATO = NM_URNA_CANDIDATO;
}
string Dados::get_NM_URNA_CANDIDATO()
{
	return NM_URNA_CANDIDATO;
}

void Dados::set_NM_SOCIAL_CANDIDATO(string NM_SOCIAL_CANDIDATO)
{
	this -> NM_SOCIAL_CANDIDATO = NM_SOCIAL_CANDIDATO;
}
string Dados::get_NM_SOCIAL_CANDIDATO()
{
	return NM_SOCIAL_CANDIDATO;
}

void Dados::set_NR_CPF_CANDIDATO(string NR_CPF_CANDIDATO)
{
	this -> NR_CPF_CANDIDATO = NR_CPF_CANDIDATO;
}
string Dados::get_NR_CPF_CANDIDATO()
{
	return NR_CPF_CANDIDATO;	
}
	        
void Dados::set_NM_EMAIL(string NM_EMAIL)
{
	this -> NM_EMAIL = NM_EMAIL;
}
string Dados::get_NM_EMAIL()
{
	return NM_EMAIL;
}
void Dados::set_CD_SITUACAO_CANDIDATURA(string CD_SITUACAO_CANDIDATURA)
{
	this -> CD_SITUACAO_CANDIDATURA = CD_SITUACAO_CANDIDATURA;
}
string Dados::get_CD_SITUACAO_CANDIDATURA()
{
	return CD_SITUACAO_CANDIDATURA;
}
void Dados::set_DS_SITUACAO_CANDIDATURA(string DS_SITUACAO_CANDIDATURA)
{
	this -> DS_SITUACAO_CANDIDATURA = DS_SITUACAO_CANDIDATURA;
}
string Dados::get_DS_SITUACAO_CANDIDATURA()
{
	return DS_SITUACAO_CANDIDATURA;
}

void Dados::set_CD_DETALHE_SITUACAO_CAND(string CD_DETALHE_SITUACAO_CAND)
{
        this ->CD_DETALHE_SITUACAO_CAND = CD_DETALHE_SITUACAO_CAND;
}
string Dados::get_CD_DETALHE_SITUACAO_CAND()
{
        return CD_DETALHE_SITUACAO_CAND;
}

void Dados::set_DS_DETALHE_SITUACAO_CAND(string DS_DETALHE_SITUACAO_CAND)
{
        this->DS_DETALHE_SITUACAO_CAND = DS_DETALHE_SITUACAO_CAND;
}
string Dados::get_DS_DETALHE_SITUACAO_CAND()
{
        return DS_DETALHE_SITUACAO_CAND;
}

void Dados::set_TP_AGREMIACAO(string TP_AGREMIACAO)
{
        this ->TP_AGREMIACAO = TP_AGREMIACAO;
}
string Dados::get_TP_AGREMIACAO()
{
        return TP_AGREMIACAO;
}

void Dados::set_NR_PARTIDO(string NR_PARTIDO)
{
        this->NR_PARTIDO = NR_PARTIDO;
}
string Dados::get_NR_PARTIDO()
{
        return NR_PARTIDO;
}
void Dados::set_SG_PARTIDO(string SG_PARTIDO)
{
        this ->SG_PARTIDO = SG_PARTIDO;
}
string Dados::get_SG_PARTIDO()
{
        return SG_PARTIDO;
}

void Dados::set_NM_PARTIDO(string NM_PARTIDO)
{
        this->NM_PARTIDO = NM_PARTIDO;
}
string Dados::get_NM_PARTIDO()
{
        return NM_PARTIDO;
}
void Dados::set_SQ_COLIGACAO(string SQ_COLIGACAO)
{
        this ->SQ_COLIGACAO = SQ_COLIGACAO;
}
string Dados::get_SQ_COLIGACAO()
{
        return SQ_COLIGACAO;
}

void Dados::set_NM_COLIGACAO(string NM_COLIGACAO)
{
        this->NM_COLIGACAO = NM_COLIGACAO;
}
string Dados::get_NM_COLIGACAO()
{
        return NM_COLIGACAO;
}

void Dados::set_DS_COMPOSICAO_COLIGACAO(string DS_COMPOSICAO_COLIGACAO)
{
        this->DS_COMPOSICAO_COLIGACAO = DS_COMPOSICAO_COLIGACAO;
}
string Dados::get_DS_COMPOSICAO_COLIGACAO()
{
        return DS_COMPOSICAO_COLIGACAO;
}

void Dados::set_CD_NACIONALIDADE(string CD_NACIONALIDADE)
{
        this->CD_NACIONALIDADE = CD_NACIONALIDADE;
}
string Dados::get_CD_NACIONALIDADE()
{
        return CD_NACIONALIDADE;
}
void Dados::set_DS_NACIONALIDADE(string DS_NACIONALIDADE)
{
        this ->DS_NACIONALIDADE = DS_NACIONALIDADE;
}
string Dados::get_DS_NACIONALIDADE()
{
        return DS_NACIONALIDADE;
}

void Dados::set_SG_UF_NASCIMENTO(string SG_UF_NASCIMENTO)
{
        this->SG_UF_NASCIMENTO = SG_UF_NASCIMENTO;
}
string Dados::get_SG_UF_NASCIMENTO()
{
        return SG_UF_NASCIMENTO;
}

void Dados::set_CD_MUNICIPIO_NASCIMENTO(string CD_MUNICIPIO_NASCIMENTO)
{
        this ->CD_MUNICIPIO_NASCIMENTO = CD_MUNICIPIO_NASCIMENTO;
}
string Dados::get_CD_MUNICIPIO_NASCIMENTO()
{
        return CD_MUNICIPIO_NASCIMENTO;
}
void Dados::set_DT_NASCIMENTO(string DT_NASCIMENTO)
{
        this ->DT_NASCIMENTO = DT_NASCIMENTO;
}
string Dados::get_DT_NASCIMENTO()
{
        return DT_NASCIMENTO;
}

void Dados::set_NR_IDADE_DATA_POSSE(string NR_IDADE_DATA_POSSE)
{
        this->NR_IDADE_DATA_POSSE = NR_IDADE_DATA_POSSE;
}
string Dados::get_NR_IDADE_DATA_POSSE()
{
        return NR_IDADE_DATA_POSSE;
}
void Dados::set_NR_TITULO_ELEITORAL_CANDIDATO(string NR_TITULO_ELEITORAL_CANDIDATO)
{
        this ->NR_TITULO_ELEITORAL_CANDIDATO = NR_TITULO_ELEITORAL_CANDIDATO;
}
string Dados::get_NR_TITULO_ELEITORAL_CANDIDATO()
{
        return NR_TITULO_ELEITORAL_CANDIDATO;
}

void Dados::set_CD_GENERO(string CD_GENERO)
{
        this->CD_GENERO = CD_GENERO;
}
string Dados::get_CD_GENERO()
{
        return CD_GENERO;
}
void Dados::set_DS_GENERO(string DS_GENERO)
{
        this ->DS_GENERO = DS_GENERO;
}
string Dados::get_DS_GENERO()
{
        return DS_GENERO;
}

void Dados::set_CD_GRAU_INSTRUCAO(string CD_GRAU_INSTRUCAO)
{
        this->CD_GRAU_INSTRUCAO = CD_GRAU_INSTRUCAO;
}
string Dados::get_CD_GRAU_INSTRUCAO()
{
        return CD_GRAU_INSTRUCAO;
}

void Dados::set_DS_GRAU_INSTRUCAO(string DS_GRAU_INSTRUCAO)
{
        this ->DS_GRAU_INSTRUCAO = DS_GRAU_INSTRUCAO;
}
string Dados::get_DS_GRAU_INSTRUCAO()
{
        return DS_GRAU_INSTRUCAO;
}

void Dados::set_CD_ESTADO_CIVIL(string CD_ESTADO_CIVIL)
{
        this ->CD_ESTADO_CIVIL = CD_ESTADO_CIVIL;
}
string Dados::get_CD_ESTADO_CIVIL()
{
        return CD_ESTADO_CIVIL;
}

void Dados::set_DS_ESTADO_CIVIL(string DS_ESTADO_CIVIL)
{
        this->DS_ESTADO_CIVIL = DS_ESTADO_CIVIL;
}
string Dados::get_DS_ESTADO_CIVIL()
{
        return DS_ESTADO_CIVIL;
}
void Dados::set_CD_COR_RACA(string CD_COR_RACA)
{
        this ->CD_COR_RACA = CD_COR_RACA;
}
string Dados::get_CD_COR_RACA()
{
        return CD_COR_RACA;
}

void Dados::set_DS_COR_RACA(string DS_COR_RACA)
{
        this->DS_COR_RACA = DS_COR_RACA;
}
string Dados::get_DS_COR_RACA()
{
        return DS_COR_RACA;
}

void Dados::set_CD_OCUPACAO(string CD_OCUPACAO)
{
        this->CD_OCUPACAO = CD_OCUPACAO;
}
string Dados::get_CD_OCUPACAO()
{
        return CD_OCUPACAO;
}

void Dados::set_DS_OCUPACAO(string DS_OCUPACAO)
{
        this->DS_OCUPACAO = DS_OCUPACAO;
}
string Dados::get_DS_OCUPACAO()
{
        return DS_OCUPACAO;
}

void Dados::set_NR_DESPESA_MAX_CAMPANHA(string NR_DESPESA_MAX_CAMPANHA)
{
        this ->NR_DESPESA_MAX_CAMPANHA = NR_DESPESA_MAX_CAMPANHA;
}
string Dados::get_NR_DESPESA_MAX_CAMPANHA()
{
        return NR_DESPESA_MAX_CAMPANHA;
}

void Dados::set_CD_SIT_TOT_TURNO(string CD_SIT_TOT_TURNO)
{
        this->CD_SIT_TOT_TURNO = CD_SIT_TOT_TURNO;
}
string Dados::get_CD_SIT_TOT_TURNO()
{
        return CD_SIT_TOT_TURNO;
}
void Dados::set_DS_SIT_TOT_TURNO(string DS_SIT_TOT_TURNO)
{
        this ->DS_SIT_TOT_TURNO = DS_SIT_TOT_TURNO;
}
string Dados::get_DS_SIT_TOT_TURNO()
{
        return DS_SIT_TOT_TURNO;
}

void Dados::set_ST_REELEICAO(string ST_REELEICAO)
{
        this->ST_REELEICAO = ST_REELEICAO;
}
string Dados::get_ST_REELEICAO()
{
        return ST_REELEICAO;
}

void Dados::imprime_informacoes()
{
	cout << endl;
	cout << "Nome: " << get_NOME() << endl;
	cout << "CPF: " << get_NR_CPF_CANDIDATO() << endl;
	cout << "Cargo: " << get_DS_CARGO() << endl;
	cout << "Estado/Federacaos: " << get_NM_UE() << endl;
	cout << "Numero Candidato: " << get_NR_CANDIDATO() << endl;
	cout << "Nome na Urna: " << get_NM_URNA_CANDIDATO() << endl;
	cout << "Partido: " << get_NM_PARTIDO() << endl;
	cout << "Ocupacao: " << get_CD_OCUPACAO() << endl;
	cout << "Data de nascimento: " << get_DT_NASCIMENTO()  << endl;
	cout << "Cidade de nascimento: " << get_CD_MUNICIPIO_NASCIMENTO() << endl;
	cout << "Estado Nascimeo: " << get_SG_UF_NASCIMENTO() << endl;
	cout << "Email: " << get_NM_EMAIL()  << endl;
	cout << endl;
}

