#include "urna.hpp"

int numero_eleitores()
{
	int votos;
	cout << endl << "**************************************************************************" << endl;
	cout << "			Bem vindo as eleicoes 2018" << endl;
	cout << "**************************************************************************" << endl << endl;
	cout << "Insira numero de eleitores: ";
	cin >> votos;

	return votos;
}

int menu()
{
	int op;
	cout << endl;
	cout << "**************************************************************************" << endl;
	cout << "				Menu Inicial" << endl;
	cout << "**************************************************************************" << endl;
	cout << "(1) Iniciar votacao" << endl;
	cout << "(2) Relatorio de votacao" << endl;
	cout << "(3) Listagem de informacoes do canditado" << endl;
	cout << "(4) Resultado eleicoes" << endl;
	cout << "(0) Sair" << endl << endl;
	cout << "Digite uma opção: ";
	cin >> op;

	return op;
}
void menu_urna()
{
	cout << endl;
	cout << "**************************************************************************" << endl;
	cout << "				Urna Eletronica" << endl;
	cout << "**************************************************************************" << endl;
	cout << "(1) Apagar caracteres" << endl;
	cout << "(2) Cancelar passo" << endl;
	cout << "(3) Confirma" << endl;
	cout << "(4) Branco - Digite 4 no código e depois confirme" << endl;
	cout << "(5) Cancela" << endl;

	cout << endl;
}


int loop_votacao(Dados *politicos)
{
	int eleitores, nr_eleitor = 0, opcao;
	bool var[5];
	string codigo, nome_eleitor, cargo[] = {"DEPUTADO DISTRITAL", "DEPUTADO FEDERAL", "SENADOR", "GOVERNADOR", "PRESIDENTE"}, nome;
	int cd_cargo[5] = {8, 6, 5, 3, 1};
	int opcao_urna;
	int resultado[1263];

	eleitores = numero_eleitores();
	system("clear");

	Eleitor *eleitor = new Eleitor[eleitores];	

	for(int i = 0; i < 1263; i++) resultado[i] = 0;

	do
	{

		opcao = menu();

		switch(opcao)
		{
			case 0:
				return 0;
			case 1:
				if(nr_eleitor == eleitores)
				{
					cout << endl;
					cout << "-> Numero de eleitores chegou ao limite" << endl;
					break;
				}

				cout << "Digite seu nome: ";
				cin >> nome;
				eleitor[nr_eleitor].set_NOME(nome);
				system("clear");

				menu_urna();

				for(int b = 0; b < 5; b++) var[b] = true;

				for(int j = 0; j < 5; j++)
				{

					cout << "Insira o codigo do " << cargo[j] << ":";
					cin >> eleitor[nr_eleitor].nr_candidato[j];
					cout << "Digite uma opcao do menu acima: ";
					cin >> opcao_urna;

					if(opcao_urna == 1)
					{
						while( opcao_urna == 1)
						{
							cout << endl <<"====    Caracteres apagados     ====" << endl << endl;
							cout << "Insira o codigo do " << cargo[j] << ":";
							cin >> eleitor[nr_eleitor].nr_candidato[j];
							cout << "Digite uma opcao do menu acima: ";
							cin >> opcao_urna;
						}
						if( opcao_urna == 3) cout << endl << "====    Confirmado    ====" << endl << endl;
					}
					else if(opcao_urna == 2)
					{
						cout << "====    Operacao Cancelada    ====" << endl;
						nr_eleitor--;
						break;
					}
					else if(opcao_urna == 3)
					{
						cout << endl << "====    Confirmado    ====" << endl << endl;
					}

					else if(opcao_urna == 5)
					{
						cout << "====    Operacao Cancelada    ====" << endl;
						nr_eleitor--;
						break;
					}
					else
					{
						cout << endl << "====Opcao Invalida. Digite Novamente.====" << endl << endl;
						cout << "Insira o codigo do " << cargo[j] << ":";
						cin >> eleitor[nr_eleitor].nr_candidato[j];
						cout << "Digite uma opcao do menu acima: ";
						cin >> opcao_urna;

						if( opcao_urna == 3)
							cout << endl << "====    Confirmado    ====" << endl << endl;
					}
				}

				for(int j = 0; j < 5; j++)
				{
					var[j] = !var[j];

					for(int k = 0; k < 1263; k++)
					{
						if(eleitor[nr_eleitor].nr_candidato[j] == politicos[k].get_NR_CANDIDATO())
						{
							if(politicos[k].get_CD_CARGO() == cd_cargo[j] and !var[j])
							{
								if(opcao_urna == 5 or opcao_urna == 2) break;
								else
									if(resultado[k] <= nr_eleitor+1) resultado[k]++;
							}

						}
					}
				}


				//system("clear");
				nr_eleitor++;

				break;
			case 2:
				cout << endl;
				cout << "**************************************************************************" << endl;
				cout << "				Relatorio de Votacao" << endl;
				cout << "**************************************************************************" << endl;
				if(nr_eleitor == 0)
				{
					cout << endl << "-> A votacao nao foi iniciada" << endl;
					break;
				}

				for(int i = 0; i < nr_eleitor; i++){

					eleitor[i].imprime_informacoes();
					
					for(int b = 0; b < 5; b++) var[b] = true;

					for(int j = 0; j < 5; j++)
					{
						var[j] = !var[j];

						for(int k = 0; k < 1263; k++)
						{
							if(eleitor[i].nr_candidato[j] == politicos[k].get_NR_CANDIDATO())
							{
								if(politicos[k].get_CD_CARGO() == cd_cargo[j] and !var[j])
								{
									cout << politicos[k].get_DS_CARGO() << ": " << politicos[k].get_NOME() << endl;
								}

							}
						}

						if(eleitor[i].nr_candidato[j] == 4)
							cout << cargo[j] << ": " << "VOTO EM BRANCO" << endl;
					}
				}

				break;
			case 3:
				system("clear");
				cout << endl;
				cout << "**************************************************************************" << endl;
				cout << "				Consulta ao Candidato" << endl;
				cout << "**************************************************************************" << endl;
				int codigo;
				cout << "Insira o codigo do candidato: ";
				cin >> codigo;
				cout << endl;

				for(int i = 0; i < 1263; i++)
				{
					if(codigo == politicos[i].get_NR_CANDIDATO())
						politicos[i].imprime_informacoes();
				}

				break;
			case 4:
				system("clear");
				cout << endl;
				cout << "**************************************************************************" << endl;
				cout << "				Resultado eleicoes" << endl;
				cout << "**************************************************************************" << endl;

				int aux, max;

				for(int b = 0; b < 5; b++) var[b] = true;

				if(nr_eleitor != eleitores)
				{
					cout << endl << "-> A votacao nao chegou ao termino." << endl <<"Termine os votos para que os resultados sejam demonstrados." << endl;
				}		
				
				for(int i = 0; i < 5; i++){

					var[i] = !var[i];
					max = 0;

					for(int j = 0; j < 1263; j++)
					{
						if( cd_cargo[i] == politicos[j].get_CD_CARGO() and !var[i])
						{
							if(resultado[j] > max)
							{
								max = resultado[j]; 
								aux = j;
							}
						}
					}

					if( resultado[aux] >=0 and cd_cargo[i] == politicos[aux].get_CD_CARGO() and !var[i])
					{
						if(nr_eleitor == eleitores)
						{
							cout << endl << politicos[aux].get_DS_CARGO() << ": " << politicos[aux].get_NOME() << "  |  QUANTIDADE DE VOTOS: " << resultado[aux] << endl;
						}
					}	
				}
				
				break;

			default:
				cout << endl << "Opcao Invalida. Digite novamente." << endl << endl;
		}

	}while(opcao);

	delete [] eleitor;

	return 0;
}
