#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include "Dados.hpp"

class Eleitor : public Dados //Herança da classe dados, uma vez que a classe dados possui um hall de informações inerentes a cada cidadão. O nome pode ser herdado para o eleitor.
{
	public:
		int nr_candidato[5];
		Eleitor();
		~Eleitor();
		
		void imprime_informacoes();
};


#endif
