#ifndef LEITURA_HPP
#define LEITURA_HPP

#include "Dados.hpp"
#include <vector>

extern string **aloca_memoria(int l, int c);
extern void desaloca_memoria(string **ptr, int l);
extern string **leitura(const char *caminho);
extern Dados *setters(string **cand_br, string **cand_df);
extern int *string_to_int(string **codigo, int qtd_candidatos, int pos);
extern void desaloca_vetor(Dados *p);
extern string **concatena_politicos(string **cand_br, string **cand_df, int lines_br, int total, int columns);


#endif
